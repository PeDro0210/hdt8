package com.VectorHeapJFC;

import java.util.PriorityQueue;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Vector;

public class JFCPriorityQueueTest {

    @Test
    public void testAddAndRemove() {
        Vector<Integer> vector = new Vector<>();
        PriorityQueue<Integer> heap = new PriorityQueue<>(vector);
        assertTrue(heap.isEmpty());
        
        heap.add(5);
        heap.add(3);
        heap.add(8);
        assertFalse(heap.isEmpty());
        assertEquals(3, heap.size());
        assertEquals(Integer.valueOf(3), heap.peek());
        
        assertEquals(Integer.valueOf(3), heap.remove());
        assertEquals(Integer.valueOf(5), heap.remove());
        assertEquals(Integer.valueOf(8), heap.remove());
        
        assertTrue(heap.isEmpty());
    }

}

