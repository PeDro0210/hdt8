package com.VectorHeapJFC;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.PriorityQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;

import java.util.Vector;


public class VectorHeapJFCTest {

    private PriorityQueue<Integer> heap;

    @Before
    public void setUp() {
        Vector <Integer> vector = new Vector<>();
        heap = new PriorityQueue<>(vector);
    }

    @Test
    public void testAdd() {
        heap.add(5);
        heap.add(3);
        heap.add(7);
        heap.add(1);
        heap.add(9);

        assertEquals(5, heap.size());
    }

    @Test
    public void testRemove() {
        heap.add(5);
        heap.add(3);
        heap.add(7);
        heap.add(1);
        heap.add(9);

        assertEquals(1, heap.remove().intValue());
        assertEquals(3, heap.remove().intValue());
        assertEquals(5, heap.remove().intValue());
        assertEquals(7, heap.remove().intValue());
        assertEquals(9, heap.remove().intValue());

        assertTrue(heap.isEmpty());
    }

    @Test
    public void testGetFirst() {
        heap.add(5);
        heap.add(3);
        heap.add(7);
        heap.add(1);
        heap.add(9);
        
        assertEquals(1, heap.peek().intValue());
    }

    @Test
    public void testIsEmpty() {
        assertTrue(heap.isEmpty());

        heap.add(5);

        assertFalse(heap.isEmpty());
    }

    @Test
    public void testSize() {
        assertEquals(0, heap.size());

        heap.add(5);
        heap.add(3);
        heap.add(7);

        assertEquals(3, heap.size());
    }

    @Test
    public void testClear() {
        heap.add(5);
        heap.add(3);
        heap.add(7);

        heap.clear();

        assertTrue(heap.isEmpty());
        assertEquals(0, heap.size());
    }
}