package com.VectorHeapUVG;

import com.HeapStuff.Classes.VectorHeapUVG;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.Vector;

public class UVGPriorityQueueTest {

    @Test
    public void testAddAndRemove() {
        Vector<Integer> vector = new Vector<>();
        VectorHeapUVG<Integer> heap = new VectorHeapUVG<>(vector);
        assertTrue(heap.isEmpty());
        
        heap.add(5);
        heap.add(3);
        heap.add(8);
        assertFalse(heap.isEmpty());
        assertEquals(3, heap.size());
        assertEquals(Integer.valueOf(3), heap.peek());
        
        assertEquals(Integer.valueOf(3), heap.remove());
        assertEquals(Integer.valueOf(5), heap.remove());
        assertEquals(Integer.valueOf(8), heap.remove());
        
        assertTrue(heap.isEmpty());
    }

}

