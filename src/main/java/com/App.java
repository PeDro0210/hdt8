package com;

import com.BasicObjects.Pacient;
import com.StaticClasses.FileReader;
import com.StaticClasses.Factory.VectorHeapFactory;
import com.StaticClasses.Factory.VectorHeaps;

import java.util.Vector;
import java.util.PriorityQueue;
import java.util.Scanner;

public class App{
    public static void main( String[] args ){
        // Hashmap GOAT fr fr no cap W rizz (jk)
        Vector<Pacient> pacients = FileReader.FileLoad(); //Carga los pacientes del archivo
        //Carga los pacientes en el mapa


        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("Que implementacion de VectorHeap desea usar?");
            System.out.println("1. JFC");
            System.out.println("2. UVG");
            System.out.println("3. Salir");
            int option = scanner.nextInt();
            System.out.println("\033[H\033[2J");

            if (option == 3){
                break;
            }

            if (option < 1 || option > 2){
                System.out.println("Opcion invalida");
                continue;
            }

            PriorityQueue<Pacient> heap = VectorHeapFactory.getVectorHeap(VectorHeaps.values()[option-1], pacients); //Crea el heap 

            // Muestra los pacientes en orden de prioridad
            while (!heap.isEmpty()){
                System.out.println(heap.poll());
            }
        }
        scanner.close();
    }
}
