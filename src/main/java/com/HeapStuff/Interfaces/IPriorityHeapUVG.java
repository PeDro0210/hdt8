package com.HeapStuff.Interfaces;
/**
 * La interfaz IPriorityHeapUVG define las operaciones básicas de una cola de prioridad.
 * Esta cola de prioridad almacena elementos comparables y permite acceder al elemento mínimo,
 * remover el elemento mínimo, agregar elementos, verificar si está vacía, obtener el tamaño
 * y limpiar la cola.
 *
 * @param <E> el tipo de elementos almacenados en la cola de prioridad
 */
public interface IPriorityHeapUVG<E extends Comparable<E>> {
	
	/**
	 * Devuelve el primer elemento de la cola de prioridad.
	 * 
	 * @return el elemento mínimo en la cola de prioridad
	 * @pre la cola de prioridad no está vacía
	 * @post devuelve el valor mínimo en la cola de prioridad
	 */
	public E peek();
	
	/**
	 * Remueve y devuelve el elemento mínimo de la cola de prioridad.
	 * 
	 * @return el elemento mínimo en la cola de prioridad
	 * @pre la cola de prioridad no está vacía
	 * @post devuelve y remueve el valor mínimo de la cola de prioridad
	 */
	public E remove();
	
	/**
	 * Agrega un elemento a la cola de prioridad.
	 * 
	 * @param value el elemento a agregar
	 * @pre el valor no es nulo y es comparable
	 * @post el valor es agregado a la cola de prioridad
	 */
	public boolean add(E value);
	
	/**
	 * Verifica si la cola de prioridad está vacía.
	 * 
	 * @return true si la cola de prioridad está vacía, false de lo contrario
	 * @post devuelve true si no hay elementos en la cola de prioridad
	 */
	public boolean isEmpty();
	
	/**
	 * Devuelve el tamaño de la cola de prioridad.
	 * 
	 * @return el número de elementos en la cola de prioridad
	 * @post devuelve el número de elementos en la cola de prioridad
	 */
	public int size();
	
	/**
	 * Remueve todos los elementos de la cola de prioridad.
	 * 
	 * @post remueve todos los elementos de la cola de prioridad
	 */
	public void clear();
}
