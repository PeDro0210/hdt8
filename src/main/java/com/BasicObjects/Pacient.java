package com.BasicObjects;

public class Pacient implements Comparable<Pacient> {
    private String name;
    private String condition;
    protected String priority; //will work as a key in the priority queue

    public Pacient(String name, String condition, String priority) {
        this.name = name;
        this.condition = condition;
        this.priority = priority;
    }

    public String getName() {return name;}

    public String getCondition() {return condition;}

    public String getPriority() {return priority;}

    private String DeleateNumberInPriority(String priority){
        String newPriority = "";
        for (int i = 0; i < priority.length(); i++){
            if (Character.isLetter(priority.charAt(i))){
                newPriority += priority.charAt(i);
            }
        }
        return newPriority;
    }
    @Override
    public String toString() {
        return "Paciente{" +
                "Nombre='" + name + '\'' +
                ", condition='" + condition + '\'' +
                ", Prioridad='" + DeleateNumberInPriority(priority) + '\'' +
                '}';
    }

    @Override
    public int compareTo(Pacient o) {
        return this.priority.compareTo(o.priority);
    }
}
