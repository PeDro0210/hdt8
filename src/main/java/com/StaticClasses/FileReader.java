package com.StaticClasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;

import com.BasicObjects.Pacient;

public class FileReader {
    private static String path = "src/main/Pacientes.txt";
    public static Vector<Pacient> FileLoad() {
        Vector<Pacient> pacients = new Vector<Pacient>();
        try (BufferedReader br = new BufferedReader(new java.io.FileReader(path))){
            String line;
            while ((line = br.readLine()) != null){
                String[] parts = line.split(",");
                Pacient pacient = new Pacient(parts[0], parts[1], parts[2]+pacients.size());
                pacients.add(pacient);
            }
            return pacients;
        } catch (IOException e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }
}
