package com.StaticClasses.Factory;

import java.util.PriorityQueue;
import java.util.Vector;

import com.HeapStuff.Classes.*;

public class VectorHeapFactory {
    public static <E extends Comparable<E>> PriorityQueue<E> getVectorHeap(VectorHeaps vectorHeap, Vector<E> v) {
        switch (vectorHeap) {
            //Este Factory no mas es para el programa, tonz no lo voy a hacer tan generico; por eos el string.
            case JFC:
                return new PriorityQueue<E>(v);
            case UVG:
                return new VectorHeapUVG<E>(v);
            default:
                return null;
        }
    }
}
